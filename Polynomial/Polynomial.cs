﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PolynomialObject.Exceptions;

namespace PolynomialObject
{
    public sealed class Polynomial
    {

        private readonly List<PolynomialMember> _members = new List<PolynomialMember>();
        private const double Precision = 0.00001;

        public Polynomial() { }

        public Polynomial(PolynomialMember member)
        {
            if (Math.Abs(member.Coefficient) > Precision)
                _members.Add(member);
        }

        public Polynomial(IEnumerable<PolynomialMember> members) =>
            _members.AddRange(members.Where(el => Math.Abs(el.Coefficient) > Precision));

        public Polynomial((double degree, double coefficient) member)
        {
            if (Math.Abs(member.coefficient) > Precision)
                _members.Add(new PolynomialMember(member.degree, member.coefficient));
        }

        public Polynomial(IEnumerable<(double degree, double coefficient)> members) =>
           _members.AddRange(members.Where(el => Math.Abs(el.degree) > Precision)
               .Select(el => new PolynomialMember(el.degree, el.coefficient)));

        /// <summary>
        /// The number of members in polynomial
        /// </summary>
        public int Count => _members.Count;

        /// <summary>
        /// Degree of polynomial
        /// </summary>
        public double Degree => _members.Max(el => el.Degree);

        public PolynomialMember[] ToArray() =>
            _members.ToArray();

        /// <summary>
        /// Adds new unique member to polynomial 
        /// </summary>
        /// <param name="member">The member to be added</param>
        /// <exception cref="PolynomialArgumentException"></exception>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public void AddMember(PolynomialMember member)
        {
            if (member is null)
                throw new PolynomialArgumentNullException("");
            if (_members.Find(el => Math.Abs(el.Degree - member.Degree) < Precision) is null && Math.Abs(member.Coefficient) > Precision)
                _members.Add(member);
            else if (Math.Abs(member.Coefficient) < Precision)
                throw new PolynomialArgumentException("The polynomial do not accept the members with 0.0 coefficient");
            else
                throw new PolynomialArgumentException("The polynomial already contains this degree member.");
        }

        /// <summary>
        /// Removes member of specified degree
        /// </summary>
        /// <param name="degree">The degree of member to be deleted</param>
        /// <returns>True if member has been deleted</returns>
        public bool RemoveMember(double degree)
        {

            return _members.Remove(
                _members.Find(el => Math.Abs(el.Degree - degree) < Precision)
                );
        }

        /// <summary>
        /// Searches the polynomial for a method of specified degree
        /// </summary>
        /// <param name="degree">Degree of member</param>
        /// <returns>True if polynomial contains member</returns>
        public bool ContainsMember(double degree) => !(Find(degree) is null);

        /// <summary>
        /// Finds member of specified degree
        /// </summary>
        /// <param name="degree">Degree of member</param>
        /// <returns>Returns the found member or null</returns>
        public PolynomialMember Find(double degree) => _members.Find(el => Math.Abs(el.Degree - degree) < Precision);

        public double this[double degree]
        {
            get
            {
                var el = Find(degree);
                if (el is null)
                    return 0;
                return el.Coefficient;
            }
            set
            {
                var el = _members.Find(member => Math.Abs(member.Degree - degree) < Precision);
                if (el is null && Math.Abs(value) > Precision)
                    _members.Add(new PolynomialMember(degree, value));
                else if (Math.Abs(value) > Precision)
                    el.Coefficient = value;
                else _members.Remove(el);
            }
        }

        #region Operations with polynomial
        /// <summary>
        /// Adds two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>New polynomial after adding</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public static Polynomial operator +(Polynomial a, Polynomial b)
        {
            if (a is null || b is null)
                throw new PolynomialArgumentNullException();
            var res = new Polynomial();
            foreach (var el in a._members)
                res.AddMember((PolynomialMember)el.Clone());
            foreach (var el in b._members)
                if (res.ContainsMember(el.Degree))
                {
                    var temp = res.Find(el.Degree);
                    temp.Coefficient += el.Coefficient;
                    if (Math.Abs(temp.Coefficient) < Precision)
                        res.RemoveMember(temp.Degree);
                }
                else
                    res.AddMember((PolynomialMember)el.Clone());
            return res;
        }

        /// <summary>
        /// Subtracts two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public static Polynomial operator -(Polynomial a, Polynomial b)
        {
            if (a is null || b is null)
                throw new PolynomialArgumentNullException();
            var res = new Polynomial();
            foreach (var el in a._members)
                res.AddMember((PolynomialMember)el.Clone());
            foreach (var el in b._members)
                if (res.ContainsMember(el.Degree))
                {
                    var temp = res.Find(el.Degree);
                    temp.Coefficient -= el.Coefficient;
                    if (Math.Abs(temp.Coefficient) < Precision)
                        res.RemoveMember(temp.Degree);
                }
                else
                {
                    var temp = (PolynomialMember)el.Clone();
                    temp.Coefficient *= -1;
                    res.AddMember(temp);
                }
            return res;
        }

        /// <summary>
        /// Multiplies two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>Returns new polynomial after multiplication</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public static Polynomial operator *(Polynomial a, Polynomial b)
        {
            if (a is null || b is null)
                throw new PolynomialArgumentNullException();
            var res = new Polynomial();
            foreach (var el1 in a._members)
                foreach (var el2 in b._members)
                {
                    var temp = (PolynomialMember)el1.Clone();
                    temp.Degree += el2.Degree;
                    temp.Coefficient *= el2.Coefficient;
                    if (res.ContainsMember(temp.Degree))
                    {
                        var contained = res.Find(temp.Degree);
                        contained.Coefficient += temp.Coefficient;
                        if (Math.Abs(contained.Coefficient) < Precision)
                            res.RemoveMember(contained.Degree);
                    }
                    else
                        res.AddMember(temp);
                }
            return res;
        }

        /// <summary>
        /// Adds polynomial to polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial to add</param>
        /// <returns>Returns new polynomial after adding</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public Polynomial Add(Polynomial polynomial) => this + polynomial;

        /// <summary>
        /// Subtracts polynomial from polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial to subtract</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public Polynomial Subtraction(Polynomial polynomial) => this - polynomial;

        /// <summary>
        /// Multiplies polynomial with polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial for multiplication </param>
        /// <returns>Returns new polynomial after multiplication</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public Polynomial Multiply(Polynomial polynomial) => this * polynomial;
        #endregion

        #region Only for Advanced Level
        /// <summary>
        /// Adds new unique member to polynomial from tuple
        /// </summary>
        /// <param name="member">The member to be added</param>
        /// <exception cref="PolynomialArgumentException"></exception>
        public void AddMember((double degree, double coefficient) member)
        {
            if (_members.Find(el => Math.Abs(el.Degree - member.degree) < Precision) is null && Math.Abs(member.coefficient) > Precision)
                _members.Add(new PolynomialMember(member.degree, member.coefficient));
            else if (Math.Abs(member.coefficient) < Precision)
                throw new PolynomialArgumentException("The polynomial do not accept the members with 0.0 coefficient");
            else
                throw new PolynomialArgumentException("The polynomial already contains this degree member.");
        }

        /// <summary>
        /// Adds polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after adding</returns>
        public static Polynomial operator +(Polynomial a, (double degree, double coefficient) b) =>
            a + new Polynomial(new PolynomialMember(b.degree, b.coefficient));

        /// <summary>
        /// Subtract polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        public static Polynomial operator -(Polynomial a, (double degree, double coefficient) b) =>
            a - new Polynomial(new PolynomialMember(b.degree, b.coefficient));

        /// <summary>
        /// Multiplies polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after multiplication</returns>
        public static Polynomial operator *(Polynomial a, (double degree, double coefficient) b) =>
            a * new Polynomial(new PolynomialMember(b.degree, b.coefficient));

        /// <summary>
        /// Adds tuple to polynomial
        /// </summary>
        /// <param name="member">The tuple to add</param>
        /// <returns>Returns new polynomial after adding</returns>
        public Polynomial Add((double degree, double coefficient) member) => this + member;

        /// <summary>
        /// Subtracts tuple from polynomial
        /// </summary>
        /// <param name="member">The tuple to subtract</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        public Polynomial Subtraction((double degree, double coefficient) member) => this - member;

        /// <summary>
        /// Multiplies tuple with polynomial
        /// </summary>
        /// <param name="member">The tuple for multiplication </param>
        /// <returns>Returns new polynomial after multiplication</returns>
        public Polynomial Multiply((double degree, double coefficient) member) => this * member;
        #endregion

        public override string ToString()
        {
            var builder = new StringBuilder();
            foreach (var el in _members)
                builder.Append(el);
            return builder.ToString();
        }
    }
}
