using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialObject
{
    public class PolynomialMember : ICloneable
    {
        public double Degree { get; set; }
        public double Coefficient { get; set; }

        public PolynomialMember() { }

        public PolynomialMember(double degree, double coefficient)
        {
            Degree = degree;
            Coefficient = coefficient;
        }

        public override string ToString()
        {
            return $"{(Coefficient >= 0 ? "+" : "")}{Coefficient}x^{Degree}";
        }

        public object Clone()
        {
            var temp = (PolynomialMember) MemberwiseClone();
            temp.Degree = Degree;
            temp.Coefficient = Coefficient;
            return temp;
        }
    }
}
